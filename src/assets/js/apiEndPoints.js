import api from "../../../public/config.js";

const apiBaseUrl = (api.apiBaseUrl.includes("localhost")) ? api.apiBaseUrl.replace("localhost",location.host) : api.apiBaseUrl;
const apiEndpoints = {
    host_url: apiBaseUrl.replace("api/", ""),
    home: apiBaseUrl + "/",
    login: apiBaseUrl + "login",
    logout: apiBaseUrl + "logout",
    register: apiBaseUrl + "register",
    getCustomers: apiBaseUrl + "customers",
    update: apiBaseUrl + "updateUser",
    deleteUser: apiBaseUrl + "deleteUser",
    blockUser: apiBaseUrl + "blockUser",
    unblockUser: apiBaseUrl + "unblockUser",
    addpost: apiBaseUrl + "addpost",
    getposts: apiBaseUrl + "getposts",
    getpost: apiBaseUrl + "getpost",
    editPost: apiBaseUrl + "updatepost",
    deletePost: apiBaseUrl + "deletepost",
    addUser: apiBaseUrl + "adduser",
    getUser: apiBaseUrl + "getuser",
    getblockedcustomers: apiBaseUrl + "blockedcustomers"
}
export default apiEndpoints;