import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store.js';
import VueCookies from 'vue-cookies';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import VModal from 'vue-js-modal';
import VueProgressBar from 'vue-progressbar';
import "bootstrap";
import 'bootstrap/dist/css/bootstrap.css';
import "bootstrap/dist/css/bootstrap.min.css";
import 'bootstrap-vue/dist/bootstrap-vue.css';
import apiEndpoints from "./assets/js/apiEndPoints.js";
import globalVariables from "./assets/js/global.js";
import methods from "./mixins.js";
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faUserSecret,
  faHome,
  faEllipsisV,
  faBars,
  faCartPlus,
  faUsers,
  faCaretDown,
  faFile,
  faPen,
  faTrashAlt,
  faPlus,
  faAngleDoubleRight,
  faAngleDoubleLeft,
  faSpinner,
  faTasks,
  faUser,
  faEye,
  faAngleDown,
  faTable,
  faPlayCircle
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
library.add(
  faUserSecret,
  faHome,
  faEllipsisV,
  faBars,
  faCartPlus,
  faUsers,
  faCaretDown,
  faFile,
  faPen,
  faTrashAlt,
  faPlus,
  faAngleDoubleRight,
  faAngleDoubleLeft,
  faSpinner,
  faTasks,
  faUser,
  faEye,
  faAngleDown,
  faTable,
  faPlayCircle
);
Vue.component("font-awesome-icon", FontAwesomeIcon);


const options = {
  color: '#007bff',
  failedColor: '#ff0000',
  thickness: '3px'
      // height: '2px',
      // transition: {
      //     speed: '0.2s',
      //     opacity: '0.6s',
      //     termination: 300
      // }
};
Vue.use(VueProgressBar, options);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(VModal);
Vue.use(VueCookies);
// set default config
Vue.$cookies.config('7d');

// // set global cookie
// Vue.$cookies.set('theme','default');
// Vue.$cookies.set('hover-time','1s');

Vue.config.productionTip = false


// api endpoints and headers
Vue.prototype.$api = apiEndpoints;
Vue.prototype.$apiHeaders = globalVariables.headers;


// page not found default settings
const errorMessage = {
  code: [4, 0, 1],
  msg: ""
};

//include global function with mixins
Vue.mixin({ methods });
Vue.mixin({ 
    methods: {
             hasPermission: function(permissions){
               const user = this.getUserData();
               const permit = user.permissions.filter(
                    item => (item == permissions || (item == 'Do_Anything'))
               );
                if (permit.length > 0) {
                    return true;
                } else {
                    errorMessage.msg =
                         "Access Denied, Please contact Admin for more details.";
                    return false;
                }
             },
             getErrorMessage: function() {
              return errorMessage;
             },
}})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')






