import Vue from "vue";
import Auth from "./Auth.vue";
import Router from "vue-router";
Vue.use(Router);

export default new Router ({
     mode: "history",
     base: process.env.BASE_URL,
     routes: [
        {
            path: "/",
            name: "login",
            component: () =>
                 import ("./views/auth/Login.vue")
        },
        {
            path: "/register",
            component: () =>
                 import ("./views/auth/Register.vue")
        },
        {
         path: "/auth",
         component: Auth,
         children : [
             {
                 path: "/dash",
                 name: 'dashboard',
                 component: () =>
                      import ("./views/Dashboard.vue")
             },
             {
                 path: "/home",
                 name: 'home',
                 component: () =>
                      import ("./views/post/Home.vue")
             },
             {
                path: "/addpost",
                name: "addpost",
                component: () =>
                     import ("./views/post/Add.vue")
             },
             {
                path: "/view",
                name: "view",
                component: () =>
                     import ("./views/post/Index.vue")
             },
             {
                 path: "/users",
                 name: "users",
                 component: () =>
                      import ("./views/user/Index.vue")
             },
             {
                 path: '/adduser',
                 name: 'adduser',
                 component: () =>
                      import ("./views/user/Add.vue")
             },
             {
                 path: '/edituser/:id',
                 name: 'edituser',
                 component: () =>
                      import ("./views/user/Edit.vue")
             },
             {
                 path: '/blockedusers',
                 name: 'blockeduser',
                 component: () =>
                      import ("./views/user/Blocked.vue")
             },
             {
                 path: "/editpost/:id",
                 name: "editpost",
                 component: () =>
                      import ("./views/post/Edit.vue")
             }
         ]
       },
    ]
});
