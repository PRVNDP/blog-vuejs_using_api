module.exports = {
    getUserData: function () {
        if(this.$cookies.isKey("user")){
            return this.$cookies.get("user");
        }
    },
    getAuthHeaders: function(){
        const user = this.getUserData();
        if(user!=(null || undefined)){
            this.$apiHeaders["headers"]["Authorization"] = "Bearer "+ user.access_token;
        }
        return this.$apiHeaders;
    },

    deleteEmptyKeys: function(data){
        const query = {};
        for(var key in data){
            (data[key]!='') ? query[key] = data[key] : '';
        }
        return query;
    }


}